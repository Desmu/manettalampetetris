// ==UserScript==
// @name        La Manette à Lampe Tetris
// @namespace   https://www.twitch.tv/
// @description Permet de jouer avec la lampe Tetris de xurei avec des boutons dans le chat Twitch.
// @include     https://www.twitch.tv/xurei
// @include     https://www.twitch.tv/popout/xurei/chat*
// @include     https://www.twitch.tv/var_a
// @include     https://www.twitch.tv/popout/var_a/chat*
// @version     2.0.0
// @grant       none
// @author      Desmu & KazuAlex
// @homepageURL https://gitlab.com/Desmu/manettalampetetris
// @updateURL   https://gitlab.com/Desmu/manettalampetetris/-/raw/main/manettalampetetris.user.js
// @downloadURL https://gitlab.com/Desmu/manettalampetetris/-/raw/main/manettalampetetris.user.js
// ==/UserScript==

const tetrisLampDebugChars = false;

const tetrisLampIcon = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gQ3JlYXRlZCB3aXRoIElua3NjYXBlIChodHRwOi8vd3d3Lmlua3NjYXBlLm9yZy8pIC0tPgoKPHN2ZwogICB3aWR0aD0iMjE3LjcyNDk5bW0iCiAgIGhlaWdodD0iMjE3LjcyNDk5bW0iCiAgIHZpZXdCb3g9IjAgMCAyMTcuNzI0OTkgMjE3LjcyNDk5IgogICB2ZXJzaW9uPSIxLjEiCiAgIGlkPSJzdmc1IgogICBpbmtzY2FwZTp2ZXJzaW9uPSIxLjEuMSAoM2JmNWFlMGQyNSwgMjAyMS0wOS0yMCkiCiAgIHNvZGlwb2RpOmRvY25hbWU9ImRyYXdpbmcuc3ZnIgogICBpbmtzY2FwZTpleHBvcnQtZmlsZW5hbWU9IkM6XFVzZXJzXEFsZXhhbmRyZVxQaWN0dXJlc1xNYW5ldHRlVGV0cmlzXGRyYXdpbmcucG5nIgogICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgIGlua3NjYXBlOmV4cG9ydC15ZHBpPSI5NiIKICAgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiCiAgIHhtbG5zOnNvZGlwb2RpPSJodHRwOi8vc29kaXBvZGkuc291cmNlZm9yZ2UubmV0L0RURC9zb2RpcG9kaS0wLmR0ZCIKICAgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIGlkPSJuYW1lZHZpZXc3IgogICAgIHBhZ2Vjb2xvcj0iIzUwNTA1MCIKICAgICBib3JkZXJjb2xvcj0iI2VlZWVlZSIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjAiCiAgICAgaW5rc2NhcGU6cGFnZW9wYWNpdHk9IjAiCiAgICAgaW5rc2NhcGU6cGFnZWNoZWNrZXJib2FyZD0iMCIKICAgICBpbmtzY2FwZTpkb2N1bWVudC11bml0cz0ibW0iCiAgICAgc2hvd2dyaWQ9ImZhbHNlIgogICAgIHNob3dib3JkZXI9ImZhbHNlIgogICAgIGlua3NjYXBlOnpvb209IjAuNSIKICAgICBpbmtzY2FwZTpjeD0iOTEiCiAgICAgaW5rc2NhcGU6Y3k9IjQ2OSIKICAgICBpbmtzY2FwZTp3aW5kb3ctd2lkdGg9IjE5MjAiCiAgICAgaW5rc2NhcGU6d2luZG93LWhlaWdodD0iMTAwOSIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iMTkxMiIKICAgICBpbmtzY2FwZTp3aW5kb3cteT0iLTgiCiAgICAgaW5rc2NhcGU6d2luZG93LW1heGltaXplZD0iMSIKICAgICBpbmtzY2FwZTpjdXJyZW50LWxheWVyPSJsYXllcjEiIC8+CiAgPGRlZnMKICAgICBpZD0iZGVmczIiPgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQxMDk1MCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNmZjhkOGQ7c3RvcC1vcGFjaXR5OjAuOTg0MzEzNzMiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3AxMDk0NiIgLz4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2Y5MjgyODtzdG9wLW9wYWNpdHk6MSIKICAgICAgICAgb2Zmc2V0PSIxIgogICAgICAgICBpZD0ic3RvcDEwOTQ4IiAvPgogICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDxsaW5lYXJHcmFkaWVudAogICAgICAgaW5rc2NhcGU6Y29sbGVjdD0iYWx3YXlzIgogICAgICAgaWQ9ImxpbmVhckdyYWRpZW50OTk0OCI+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiM5MGZjODc7c3RvcC1vcGFjaXR5OjEiCiAgICAgICAgIG9mZnNldD0iMCIKICAgICAgICAgaWQ9InN0b3A5OTQ0IiAvPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojNGNlMjRjO3N0b3Atb3BhY2l0eToxOyIKICAgICAgICAgb2Zmc2V0PSIwLjQxMjE2MjM2IgogICAgICAgICBpZD0ic3RvcDk5NTAiIC8+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiMwOGM4MTI7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjEiCiAgICAgICAgIGlkPSJzdG9wOTk0NiIgLz4KICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8bGluZWFyR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIGlkPSJsaW5lYXJHcmFkaWVudDgzNTYiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojNmM4NGZmO3N0b3Atb3BhY2l0eToxIgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wODM1MiIgLz4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6IzBjMDBlNjtzdG9wLW9wYWNpdHk6MSIKICAgICAgICAgb2Zmc2V0PSIxIgogICAgICAgICBpZD0ic3RvcDgzNTQiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQ1MDU5Ij4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2Y3YzNmYztzdG9wLW9wYWNpdHk6MSIKICAgICAgICAgb2Zmc2V0PSIwIgogICAgICAgICBpZD0ic3RvcDUwNTUiIC8+CiAgICAgIDxzdG9wCiAgICAgICAgIHN0eWxlPSJzdG9wLWNvbG9yOiNlZTYyZjk7c3RvcC1vcGFjaXR5OjE7IgogICAgICAgICBvZmZzZXQ9IjAuNDg4NDA5OTciCiAgICAgICAgIGlkPSJzdG9wODg3MiIgLz4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2VhMTZmYTtzdG9wLW9wYWNpdHk6MSIKICAgICAgICAgb2Zmc2V0PSIxIgogICAgICAgICBpZD0ic3RvcDUwNTciIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPGxpbmVhckdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICBpZD0ibGluZWFyR3JhZGllbnQxMjg2LTEiPgogICAgICA8c3RvcAogICAgICAgICBzdHlsZT0ic3RvcC1jb2xvcjojZWZlY2M4O3N0b3Atb3BhY2l0eTowLjk4NDMxMzczIgogICAgICAgICBvZmZzZXQ9IjAiCiAgICAgICAgIGlkPSJzdG9wMzQ3OCIgLz4KICAgICAgPHN0b3AKICAgICAgICAgc3R5bGU9InN0b3AtY29sb3I6I2ZmZTgwMztzdG9wLW9wYWNpdHk6MSIKICAgICAgICAgb2Zmc2V0PSIxIgogICAgICAgICBpZD0ic3RvcDM0ODAiIC8+CiAgICA8L2xpbmVhckdyYWRpZW50PgogICAgPHJhZGlhbEdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ5OTQ4IgogICAgICAgaWQ9InJhZGlhbEdyYWRpZW50MjI2MCIKICAgICAgIGN4PSIxMTEuOTc1MTkiCiAgICAgICBjeT0iMTQ1LjI4MjQ2IgogICAgICAgZng9IjExMS45NzUxOSIKICAgICAgIGZ5PSIxNDUuMjgyNDYiCiAgICAgICByPSIzNi45NTQxNjYiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoLTEuNjQxMDAzNywtMS43OTgyODUxLDEuMjQzMjE5OCwtMS4xMzQ1MDU0LDUwLjc1ODY4NSw0OTAuNTQxMikiIC8+CiAgICA8cmFkaWFsR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDEwOTUwIgogICAgICAgaWQ9InJhZGlhbEdyYWRpZW50MjI2MC05IgogICAgICAgY3g9IjEyNC40ODI5MyIKICAgICAgIGN5PSIxNjQuMzYxNDMiCiAgICAgICBmeD0iMTI0LjQ4MjkzIgogICAgICAgZnk9IjE2NC4zNjE0MyIKICAgICAgIHI9IjM2Ljk1NDE2NiIKICAgICAgIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIgogICAgICAgZ3JhZGllbnRUcmFuc2Zvcm09Im1hdHJpeCgtMC44MTI0MTcyMiwtMC44NzIwNDg3MiwwLjgzNzY5Mjk1LC0wLjc4MDQxNTI2LDg3LjgwMTgxNywzMzUuMzgyNjQpIiAvPgogICAgPHJhZGlhbEdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQxMjg2LTEiCiAgICAgICBpZD0icmFkaWFsR3JhZGllbnQyMjYwLTktMiIKICAgICAgIGN4PSI5Ni44NTAzOTUiCiAgICAgICBjeT0iMTQ3LjkxODI3IgogICAgICAgZng9Ijk2Ljg1MDM5NSIKICAgICAgIGZ5PSIxNDcuOTE4MjciCiAgICAgICByPSIzNi45NTQxNjYiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoLTEuNTkyNjY4MSwtMS41MzA5NSwxLjAxMzg5OCwtMS4wNTQ3ODY1LDIxMi4wNTI5Myw0NDEuNzQ1NjYpIiAvPgogICAgPHJhZGlhbEdyYWRpZW50CiAgICAgICBpbmtzY2FwZTpjb2xsZWN0PSJhbHdheXMiCiAgICAgICB4bGluazpocmVmPSIjbGluZWFyR3JhZGllbnQ4MzU2IgogICAgICAgaWQ9InJhZGlhbEdyYWRpZW50MjI2MC05LTAiCiAgICAgICBjeD0iMTA0LjU3MjA3IgogICAgICAgY3k9IjE0My41MjQ4OSIKICAgICAgIGZ4PSIxMDQuNTcyMDciCiAgICAgICBmeT0iMTQzLjUyNDg5IgogICAgICAgcj0iMzYuOTU0MTY2IgogICAgICAgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiCiAgICAgICBncmFkaWVudFRyYW5zZm9ybT0ibWF0cml4KC0xLjAzMDcwNTksLTEuMDQ0OTI4NiwxLjA0MTYwNCwtMS4wMjc0MzYsODIuNjIwMDI4LDQ2MS41MzY5OSkiIC8+CiAgICA8cmFkaWFsR3JhZGllbnQKICAgICAgIGlua3NjYXBlOmNvbGxlY3Q9ImFsd2F5cyIKICAgICAgIHhsaW5rOmhyZWY9IiNsaW5lYXJHcmFkaWVudDUwNTkiCiAgICAgICBpZD0icmFkaWFsR3JhZGllbnQyMjYwLTktMC0yIgogICAgICAgY3g9IjExMi42OTgwNyIKICAgICAgIGN5PSIxNTguNzUwNSIKICAgICAgIGZ4PSIxMTIuNjk4MDciCiAgICAgICBmeT0iMTU4Ljc1MDUiCiAgICAgICByPSIzNi45NTQxNjYiCiAgICAgICBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIKICAgICAgIGdyYWRpZW50VHJhbnNmb3JtPSJtYXRyaXgoLTEuMDExNDAzMiwtMS4wNjM2MjMsMS4zMjAwNTM2LC0xLjI1NTI1ODQsMzYuMjQwNjQzLDU3MC43MTkwNSkiIC8+CiAgPC9kZWZzPgogIDxnCiAgICAgaW5rc2NhcGU6bGFiZWw9IkxheWVyIDEiCiAgICAgaW5rc2NhcGU6Z3JvdXBtb2RlPSJsYXllciIKICAgICBpZD0ibGF5ZXIxIgogICAgIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xNC4xNjg3NTUsLTc5Ljc4NTQxNikiPgogICAgPHJlY3QKICAgICAgIHN0eWxlPSJtaXgtYmxlbmQtbW9kZTpub3JtYWw7ZmlsbDp1cmwoI3JhZGlhbEdyYWRpZW50MjI2MCk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMzMzMzMzM7c3Ryb2tlLXdpZHRoOjU7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmUiCiAgICAgICBpZD0icmVjdDMxIgogICAgICAgd2lkdGg9IjcwLjkwODMzMyIKICAgICAgIGhlaWdodD0iNzAuOTA4MzMzIgogICAgICAgeD0iMTYuNjY4NzU1IgogICAgICAgeT0iODIuMjg1NDE2IgogICAgICAgcnk9IjcuNjcyOTE2OSIKICAgICAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcQWxleGFuZHJlXFBpY3R1cmVzXE1hbmV0dGVUZXRyaXNcZHJhd2luZy5wbmciCiAgICAgICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgICAgICBpbmtzY2FwZTpleHBvcnQteWRwaT0iOTYiIC8+CiAgICA8cmVjdAogICAgICAgc3R5bGU9Im1peC1ibGVuZC1tb2RlOm5vcm1hbDtmaWxsOnVybCgjcmFkaWFsR3JhZGllbnQyMjYwLTkpO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMzMzMzMzO3N0cm9rZS13aWR0aDo1O3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lIgogICAgICAgaWQ9InJlY3QzMS0xIgogICAgICAgd2lkdGg9IjcwLjkwODMzMyIKICAgICAgIGhlaWdodD0iNzAuOTA4MzMzIgogICAgICAgeD0iODcuNTc3MDg3IgogICAgICAgeT0iODIuMjg1NDE2IgogICAgICAgcnk9IjcuNjcyOTE2OSIKICAgICAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcQWxleGFuZHJlXFBpY3R1cmVzXE1hbmV0dGVUZXRyaXNcZHJhd2luZy5wbmciCiAgICAgICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgICAgICBpbmtzY2FwZTpleHBvcnQteWRwaT0iOTYiIC8+CiAgICA8cmVjdAogICAgICAgc3R5bGU9Im1peC1ibGVuZC1tb2RlOm5vcm1hbDtmaWxsOnVybCgjcmFkaWFsR3JhZGllbnQyMjYwLTktMCk7ZmlsbC1vcGFjaXR5OjE7ZmlsbC1ydWxlOmV2ZW5vZGQ7c3Ryb2tlOiMzMzMzMzM7c3Ryb2tlLXdpZHRoOjU7c3Ryb2tlLW1pdGVybGltaXQ6NDtzdHJva2UtZGFzaGFycmF5Om5vbmUiCiAgICAgICBpZD0icmVjdDMxLTEtNCIKICAgICAgIHdpZHRoPSI3MC45MDgzMzMiCiAgICAgICBoZWlnaHQ9IjcwLjkwODMzMyIKICAgICAgIHg9Ijg3LjU3NzA4IgogICAgICAgeT0iMTUzLjE5Mzc2IgogICAgICAgcnk9IjcuNjcyOTE2OSIKICAgICAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcQWxleGFuZHJlXFBpY3R1cmVzXE1hbmV0dGVUZXRyaXNcZHJhd2luZy5wbmciCiAgICAgICBpbmtzY2FwZTpleHBvcnQteGRwaT0iOTYiCiAgICAgICBpbmtzY2FwZTpleHBvcnQteWRwaT0iOTYiIC8+CiAgICA8cmVjdAogICAgICAgc3R5bGU9Im1peC1ibGVuZC1tb2RlOm5vcm1hbDtmaWxsOnVybCgjcmFkaWFsR3JhZGllbnQyMjYwLTktMC0yKTtmaWxsLW9wYWNpdHk6MTtmaWxsLXJ1bGU6ZXZlbm9kZDtzdHJva2U6IzMzMzMzMztzdHJva2Utd2lkdGg6NTtzdHJva2UtbWl0ZXJsaW1pdDo0O3N0cm9rZS1kYXNoYXJyYXk6bm9uZSIKICAgICAgIGlkPSJyZWN0MzEtMS00LTkiCiAgICAgICB3aWR0aD0iNzAuOTA4MzMzIgogICAgICAgaGVpZ2h0PSI3MC45MDgzMzMiCiAgICAgICB4PSI4Ny41NzcwOCIKICAgICAgIHk9IjIyNC4xMDIwOCIKICAgICAgIHJ5PSI3LjY3MjkxNjkiCiAgICAgICBpbmtzY2FwZTpleHBvcnQtZmlsZW5hbWU9IkM6XFVzZXJzXEFsZXhhbmRyZVxQaWN0dXJlc1xNYW5ldHRlVGV0cmlzXGRyYXdpbmcucG5nIgogICAgICAgaW5rc2NhcGU6ZXhwb3J0LXhkcGk9Ijk2IgogICAgICAgaW5rc2NhcGU6ZXhwb3J0LXlkcGk9Ijk2IiAvPgogICAgPHJlY3QKICAgICAgIHN0eWxlPSJtaXgtYmxlbmQtbW9kZTpub3JtYWw7ZmlsbDp1cmwoI3JhZGlhbEdyYWRpZW50MjI2MC05LTIpO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpldmVub2RkO3N0cm9rZTojMzMzMzMzO3N0cm9rZS13aWR0aDo1O3N0cm9rZS1taXRlcmxpbWl0OjQ7c3Ryb2tlLWRhc2hhcnJheTpub25lIgogICAgICAgaWQ9InJlY3QzMS0xLTYiCiAgICAgICB3aWR0aD0iNzAuOTA4MzMzIgogICAgICAgaGVpZ2h0PSI3MC45MDgzMzMiCiAgICAgICB4PSIxNTguNDg1NDEiCiAgICAgICB5PSI4Mi4yODU0MTYiCiAgICAgICByeT0iNy42NzI5MTY5IgogICAgICAgaW5rc2NhcGU6ZXhwb3J0LWZpbGVuYW1lPSJDOlxVc2Vyc1xBbGV4YW5kcmVcUGljdHVyZXNcTWFuZXR0ZVRldHJpc1xkcmF3aW5nLnBuZyIKICAgICAgIGlua3NjYXBlOmV4cG9ydC14ZHBpPSI5NiIKICAgICAgIGlua3NjYXBlOmV4cG9ydC15ZHBpPSI5NiIgLz4KICA8L2c+Cjwvc3ZnPgo=";

const tetrisLampChars = {
    "0": "..1111...1....1..1....1..1....1..1....1..1....1...1111..",
    "1": "....1......11.....1.1.......1.......1.......1.......1...",
    "2": ".111111.1......1.....11....11....11.....1.......11111111",
    "3": ".111111.1......1.......1..11111........11......1.111111.",
    "4": ".....1......11.....1.1....1..1...1111111.....1.......1..",
    "5": "111111111.......1.......1111111........11......1.111111.",
    "6": ".111111.1......11.......1111111.1......11......1.111111.",
    "7": "11111111......11.....11.....11....11.....11.....11......",
    "8": ".111111.1......11......1.111111.1......11......1.111111.",
    "9": ".111111.1......11......1.1111111.......11......1.111111.",
    " ": "........................................................",
    "a": ".111111.1......11......1111111111......11......11......1",
    "b": "1111111.1......11......11111111.1......11......11111111.",
    "c": ".111111.1......11.......1.......1.......1......1.111111.",
    "d": "1111111.1......11......11......11......11......11111111.",
    "e": "111111111.......1.......111111111.......1.......11111111",
    "f": "111111111.......1.......1111111.1.......1.......1.......",
    "g": ".11111111.......1.......1..1111.1......11......1.111111.",
    "h": "1......11......11......1111111111......11......11......1",
    "i": "...11..............11......11......11......11......11...",
    "j": "......1...............1.......1.......1..1....1...1111..",
    "k": "1.....111...11..1.11....11......1.11....1...11..1.....11",
    "l": "1.......1.......1.......1.......1.......1.......11111111",
    "m": "1......111....111.1..1.11..11..11......11......11......1",
    "n": "11.....1111....11.11...11..11..11...11.11....1111.....11",
    "o": ".111111.1......11......11......11......11......1.111111.",
    "p": "1111111.1......11......11111111.1.......1.......1.......",
    "q": ".111111.1......11......11...1..11....1.11.....11.1111111",
    "r": "1111111.1......11......11111111.1.11....1...11..1.....11",
    "s": ".111111.1......11........111111........11......1.111111.",
    "t": "1111111111111111...11......11......11......11......11...",
    "u": "1......11......11......11......11......11......1.111111.",
    "v": "1......11......1.1....1..1....1...1..1....1..1.....11...",
    "w": "1......11......11......11..11..11.1..1.111....111......1",
    "x": "1......1.1....1...1..1.....11.....1..1...1....1.1......1",
    "y": "11....1111....11.11..11...1111.....11......11......11...",
    "z": "11111111.....11.....11.....11.....11.....11.....11111111"
};

const tetrisLampStaticGridColors = [
    ["#ff00ff", "#00ff00", "#00ff00", "#00ffff", "#00ffff", "#00ffff", "#00ffff", "#ff00ff"],
    ["#ff00ff", "#ff00ff", "#00ff00", "#00ff00", "#ff0000", "#ff0000", "#ff00ff", "#ff00ff"],
    ["#ff00ff", "#ff0000", "#ff0000", "#00ff00", "#00ff00", "#ff0000", "#ff0000", "#ff00ff"],
    ["#ff0000", "#ff0000", "#00ff00", "#00ff00", "#00ffff", "#0000ff", "#0000ff", "#0000ff"],
    ["#ffff00", "#ffff00", "#0000ff", "#0000ff", "#00ffff", "#ff8800", "#ff8800", "#0000ff"],
    ["#ffff00", "#ffff00", "#ff8800", "#0000ff", "#00ffff", "#ff8800", "#ffff00", "#ffff00"],
    ["#ff8800", "#ff8800", "#ff8800", "#0000ff", "#00ffff", "#ff8800", "#ffff00", "#ffff00"]
];

const tetrisLampGridPremade = {
    "smile": ".....1...1....1.......1....11.1.......1..1....1......1..",
    "wink": ".....1...1....1.......1....11.1..1....1..1....1.1....1..",
    "heart": ".11..11.111111111111111111111111.111111...1111.....11...",
    "cross": "1.....1..1...1....1.1......1......1.1....1...1..1.....1.",
    "oui": [
        ".111111.1......11......11......11......11......1.111111.",
        "1......11......11......11......11......11......1.111111.",
        "...11..............11......11......11......11......11..."
    ],
    "non": [
        "11.....1111....11.11...11..11..11...11.11....1111.....11",
        ".111111.1......11......11......11......11......1.111111.",
        "11.....1111....11.11...11..11..11...11.11....1111.....11"
    ]
};

function minifyString (str) {
    return str.trim().replace(/\r?\n|\r/g, " ").replace(/ {2}/g, "");
}

function getStreamerName () {
    const urlParams = window.location.href.split("/");
    if (urlParams[urlParams.length - 1] === "") {
        urlParams.pop();
    }
    let streamerName = "";
    if (window.location.href.indexOf("/popout/") === -1) {
        streamerName = urlParams[urlParams.length - 1];
    } else {
        streamerName = urlParams[urlParams.length - 2];
    }
    return streamerName;
}

function sendMessage (message, sendable = true) {
    document.querySelector("[data-a-target='chat-input']").focus();
    Object.getOwnPropertyDescriptor(window.HTMLTextAreaElement.prototype, "value").set.call(document.querySelector("[data-a-target='chat-input']"), message);
    if (sendable === true) {
        document.querySelector("[data-a-target='chat-input']").dispatchEvent(new Event("input", {"bubbles": true}));
        document.querySelector("[data-a-target='chat-send-button']").click();
        document.querySelector("[data-a-target='chat-input']").blur();
    }
}

function tetrisLampCreateStyle () {
    const styleTag = document.createElement("style");
    styleTag.innerText = minifyString(`
        .tetris-lamp-toggle {
            border-radius: var(--border-radius-medium);
            height: var(--button-size-default);
            margin-left: 0.5rem;
            width: var(--button-size-default);
        }
        .tetris-lamp-toggle:hover {
            background-color: var(--color-background-button-text-hover);
        }
        .tetris-lamp-icon {
            cursor: pointer;
            display: inline;
            height: 100%;
            image-rendering: pixelated;
            padding: 5px;
            width: 100%;
        }
        .tetris-lamp-button {
            background-color: var(--color-background-button-primary-default);
            border-radius: var(--border-radius-medium);
            color: var(--color-text-button-primary);
            cursor: pointer;
            display: inline-block;
            font-weight: var(--font-weight-semibold);
            margin: 5px;
            padding: 5px;
            text-align: center;
        }
        .tetris-lamp-button:hover,
        .tetris-lamp-button-focused {
            background-color: var(--color-background-button-primary-hover);
        }
        .tetris-lamp-square {
            align-items: center;
            display: flex;
            flex-direction: column;
            justify-content: center;
            min-width: 176px;
        }
        .tetris-lamp.tetris-lamp-hidden {
            margin-top: 0;
            max-height: 0;
            transition: max-height .15s ease-out;
        }
        .tetris-lamp {
            display: flex;
            flex-direction: column;
            height: auto;
            max-height: 350px;
            overflow: hidden;
            transition: max-height .15s ease-in;
        }
        @media (min-width: 340px) {
            .tetris-lamp {
                margin-top: 20px;
            }
            .tetris-lamp-main {
                flex-direction: column;
            }
        }
        .tetris-lamp-main {
            display: flex;
            flex-direction: row;
            justify-content: flex-end;
        }
        .tetris-lamp-premade {
            align-items: center;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: flex-end;
        }
        .tetris-lamp-actions {
            align-content: flex-end;
            display: flex;
            flex-direction: column;
        }
        .tetris-lamp-square__row {
            display: flex;
            flex-direction: row;
            height: 22px;
            justify-content: center;
        }
        .tetris-lamp-cell {
            clip: rect(0,0,0,0);
            pointer-events: none;
            position: absolute;
        }
        .tetris-lamp-square__cell {
            border: 1px solid #fff;
            cursor: pointer;
            display: inline-block;
            height: 22px;
            transition: background-color .3s cubic-bezier(.22,.61,.36,1);
            width: 22px;
        }
        .tw-root--theme-dark .tetris-lamp-square__cell {
            border: 1px solid #18181b;
        }
        .tetris-lamp-cell:not(:checked) + .tetris-lamp-square__cell:hover,
        .tw-root--theme-dark .tetris-lamp-cell:not(:checked) + .tetris-lamp-square__cell:hover {
            background-color: var(--color-background-button-primary-default) !important;
        }

        .tetris-lamp-cell:not(:checked) + .tetris-lamp-square__cell {
            background-color: #e5e5e5 !important;
        }
        .tw-root--theme-dark .tetris-lamp-cell:not(:checked) + .tetris-lamp-square__cell {
            background-color: #464649 !important;
        }
    `);
    document.querySelector("head").append(styleTag);
}

function tetrisLampDisplayOnGrid (chars) {
    let index = 0;
    document.querySelectorAll(".tetris-lamp-cell").forEach((el) => {
        el.checked = chars[index] !== ".";
        index += 1;
    });
}

function tetrisLampSendMessageMulti (messages, needToDisplayOnGrid = false) {
    if (!messages || !messages.length) {
        return;
    }
    let pos = 0;
    let textInterval = null;
    const localFunc = () => {
        sendMessage(`!tetris ${messages[pos]}`);
        if (needToDisplayOnGrid) {
            tetrisLampDisplayOnGrid(messages[pos]);
        }
        pos += 1;
        if (pos >= messages.length) {
            clearInterval(textInterval);
        }
    };
    localFunc();
    if (messages.length > 1) {
        textInterval = setInterval(localFunc, 1000);
    }
}

function tetrisLampHandlePremadeButtonClicked ($e) {
    if ($e.target && $e.target.attributes && $e.target.attributes["data-tetris-lamp-value"]) {
        const {value} = $e.target.attributes["data-tetris-lamp-value"];
        let chars = null;
        if (tetrisLampGridPremade[value]) {
            chars = tetrisLampGridPremade[value];
        } else if (tetrisLampChars[value]) {
            chars = tetrisLampChars[value];
        }
        if (chars) {
            if (Array.isArray(chars)) {
                tetrisLampSendMessageMulti(chars, true);
            } else {
                tetrisLampDisplayOnGrid(chars);
                sendMessage(`!tetris ${chars}`);
            }
        }
    }
}

function tetrisLampToggle () {
    document.querySelectorAll(".tetris-lamp").forEach((tag) => {
        tag.classList.toggle("tetris-lamp-hidden");
    });
}

function tetrisLampHandleToggleButtonClicked () {
    tetrisLampToggle();
}

function tetrisLampSendTextGrid () {
    let textToSend = document.querySelector("[data-a-target='chat-input']").value;
    if (textToSend) {
        textToSend = textToSend.toLowerCase().normalize("NFD").replace(/\p{Diacritic}/gu, "");
    }
    if (textToSend) {
        const textToTetris = [];
        for (const character of Array.from(textToSend)) {
            if (tetrisLampChars[character]) {
                textToTetris.push(tetrisLampChars[character]);
            }
        }
        tetrisLampSendMessageMulti(textToTetris, true);
    }
}

function tetrisLampHandleMessageBtn () {
    tetrisLampSendTextGrid();
}

function tetrisLampSendGrid () {
    let tetrisGrid = "";
    document.querySelectorAll(".tetris-lamp-cell").forEach((cell) => {
        if (cell.checked) {
            tetrisGrid += "1";
        } else {
            tetrisGrid += ".";
        }
    });
    sendMessage(`!tetris ${tetrisGrid}`);
}

function tetrisLampHandleGridBtnClicked () {
    tetrisLampSendGrid();
}

function tetrisLampClearGrid () {
    document.querySelectorAll(".tetris-lamp-cell").forEach((cell) => {
        cell.checked = false;
    });
}

function tetrisLampHandleGridClearBtnClicked () {
    tetrisLampClearGrid();
}

function tetrisLampFillGrid () {
    document.querySelectorAll(".tetris-lamp-cell").forEach((cell) => {
        cell.checked = true;
    });
}

function tetrisLampHandleGridFillBtnClicked () {
    tetrisLampFillGrid();
}

function tetrisLampCreateButton () {
    const btnTetrisToggle = document.createElement("button");
    btnTetrisToggle.classList.add("tetris-lamp-toggle");
    btnTetrisToggle.addEventListener("click", tetrisLampHandleToggleButtonClicked);
    const imgTetrisIcon = document.createElement("img");
    imgTetrisIcon.classList.add("tetris-lamp-icon");
    imgTetrisIcon.setAttribute("src", tetrisLampIcon);
    imgTetrisIcon.setAttribute("alt", "Manettatetris");
    btnTetrisToggle.append(imgTetrisIcon);
    document.querySelector(".chat-input__buttons-container > div:last-child").append(btnTetrisToggle);
}

function tetrisLampCreateDiv () {
    const div = document.createElement("div");
    div.classList.add("tetris-lamp", "tetris-lamp-hidden");
    const divMain = document.createElement("div");
    divMain.classList.add("tetris-lamp-main");
    let varADivHTML = "";
    if (getStreamerName() === "var_a") {
        varADivHTML = "<button type='button' class='tetris-lamp-message-btn tetris-lamp-button'>Poster le message du chat</button>";
    }
    let divHTML = `
        <div class="tetris-lamp-actions">
            ${varADivHTML}
            <button type="button" class="tetris-lamp-grid-btn tetris-lamp-button">Poster la grille</button>
            <button type="button" class="tetris-lamp-grid-clear-btn tetris-lamp-button">Vider la grille</button>
            <button type="button" class="tetris-lamp-grid-fill-btn tetris-lamp-button">Remplir la grille</button>
        </div>
    `;
    let inputs = "";
    let inputsHTML = "";
    for (let row = 0; row < 7; row += 1) {
        for (let col = 0; col < 8; col += 1) {
            inputs += `
                <input type="checkbox" id="tetris-lamp-cell-${row}-${col}" class="tetris-lamp-cell" />
                <label for="tetris-lamp-cell-${row}-${col}" class="tetris-lamp-square__cell" style="background-color: ${tetrisLampStaticGridColors[row][col]}"></label>
            `;
        }
        inputsHTML += `<div class="tetris-lamp-square__row">${inputs}</div>`;
        inputs = "";
    }
    divHTML += `<div class="tetris-lamp-square">${inputsHTML}</div>`;
    divMain.innerHTML = divHTML;
    div.append(divMain);
    if (div.querySelector(".tetris-lamp-message-btn")) {
        div.querySelector(".tetris-lamp-message-btn").addEventListener("click", tetrisLampHandleMessageBtn);
    }
    div.querySelector(".tetris-lamp-grid-btn").addEventListener("click", tetrisLampHandleGridBtnClicked);
    div.querySelector(".tetris-lamp-grid-clear-btn").addEventListener("click", tetrisLampHandleGridClearBtnClicked);
    div.querySelector(".tetris-lamp-grid-fill-btn").addEventListener("click", tetrisLampHandleGridFillBtnClicked);
    const divPremadeButtons = document.createElement("div");
    divPremadeButtons.classList.add("tetris-lamp-premade");
    divPremadeButtons.innerHTML = `
        <button type="button" class="tetris-lamp-button tetris-lamp-premade-btn" data-tetris-lamp-value="smile">:-)</button>
        <button type="button" class="tetris-lamp-button tetris-lamp-premade-btn" data-tetris-lamp-value="wink">;-)</button>
        <button type="button" class="tetris-lamp-button tetris-lamp-premade-btn" data-tetris-lamp-value="heart"><3</button>
        <button type="button" class="tetris-lamp-button tetris-lamp-premade-btn" data-tetris-lamp-value="oui">OUI</button>
        <button type="button" class="tetris-lamp-button tetris-lamp-premade-btn" data-tetris-lamp-value="non">NON</button>
    `;
    if (tetrisLampDebugChars === true) {
        Object.entries(tetrisLampChars).forEach(([normalChar, tetrisChar]) => {
            divPremadeButtons.innerHTML += `<button type='button' class='tetris-lamp-char-button tetris-lamp-button' data-tetris-char='${tetrisChar}'>${normalChar}</button>`;
        });
    }
    divPremadeButtons.querySelectorAll(".tetris-lamp-premade-btn").forEach((el) => {
        el.addEventListener("click", tetrisLampHandlePremadeButtonClicked);
    });
    div.append(divPremadeButtons);
    document.querySelector(".chat-input").append(div);
}

function tetrisLampCreateInterface () {
    tetrisLampCreateButton();
    tetrisLampCreateDiv();
}

function tetrisLampCheckForToggleBtn () {
    if (!document.querySelector(".tetris-lamp-toggle")) {
        tetrisLampCreateButton();
    }
}

function tetrisLampFixForFrankerFaceZ () {
    // FrankerFaceZ removes toggle button from DOM, so we listen for mutations to read when a node is removed.
    const MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
    const mutations = new MutationObserver((moEvent) => {
        if (moEvent && moEvent.length && moEvent[0].removedNodes && moEvent[0].removedNodes.length) {
            tetrisLampCheckForToggleBtn();
        }
    });
    mutations.observe(document.body, {"childList": true, "subtree": true});
}

window.addEventListener("load", () => {
    tetrisLampCreateStyle();
    tetrisLampCreateInterface();
    tetrisLampFixForFrankerFaceZ();
    if (tetrisLampDebugChars === true) {
        document.querySelectorAll(".tetris-lamp-char-button").forEach((charBtn) => {
            charBtn.addEventListener("click", () => {
                const tetrisCells = document.querySelectorAll(".tetris-lamp-cell");
                if (tetrisCells && tetrisCells.length) {
                    Array.from(charBtn.getAttribute("data-tetris-lamp-char")).forEach((charBit, index) => {
                        tetrisCells[index].checked = charBit !== ".";
                    });
                }
            });
        });
    }
});
